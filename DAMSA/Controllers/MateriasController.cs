﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAMSA.Models;

namespace DAMSA.Controllers
{
    public class MateriasController : Controller
    {
        private DAMSAContext db = new DAMSAContext();

        // GET: Materias
        public ActionResult Index()
        {
            var materias = db.Materias;
            return View(materias.ToList());
        }

        // GET: Materias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Materia materia = db.Materias.Find(id);
            if (materia == null)
            {
                return HttpNotFound();
            }
            return View(materia);
        }

        // GET: Materias/Create
        public ActionResult Create()
        {
            var model = new Materia();
            model.fch_Alta = DateTime.Now;
            model.fch_Mod = DateTime.Now;

            ViewBag.ProfesorId = new SelectList(
                CombosHelper.GetProfesors(), 
                "ProfesorId", 
                "Nombre");
            return View(model);
        }

        // POST: Materias/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MateriaId,Nombre,ProfesorId,Activo,fch_Alta,fch_Mod")] Materia materia)
        {
            if (ModelState.IsValid)
            {
                db.Materias.Add(materia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProfesorId = new SelectList(
                CombosHelper.GetProfesors(), 
                "ProfesorId", 
                "Nombre", 
                materia.ProfesorId);
            return View(materia);
        }

        // GET: Materias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var materia = new Materia();
            materia = db.Materias.Find(id);
            materia.fch_Mod = DateTime.Now;
            if (materia == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProfesorId = new SelectList(
                CombosHelper.GetProfesors(), 
                "ProfesorId", 
                "Nombre", 
                materia.ProfesorId);
            return View(materia);
        }

        // POST: Materias/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MateriaId,Nombre,ProfesorId,Activo,fch_Alta,fch_Mod")] Materia materia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(materia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProfesorId = new SelectList(
                CombosHelper.GetProfesors(), 
                "ProfesorId", 
                "Nombre", 
                materia.ProfesorId);
            return View(materia);
        }

        // GET: Materias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Materia materia = db.Materias.Find(id);
            if (materia == null)
            {
                return HttpNotFound();
            }
            return View(materia);
        }

        // POST: Materias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Materia materia = db.Materias.Find(id);
            db.Materias.Remove(materia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}