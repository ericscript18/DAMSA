﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAMSA.Models;

namespace DAMSA.Controllers
{
    public class ClasesController : Controller
    {
        private DAMSAContext db = new DAMSAContext();

        // GET: Clases
        public ActionResult Index()
        {
            var clases = db.Clases.Include(c => c.Alumno).Include(c => c.Materia).Include(c => c.Profesor);
            return View(clases.ToList());
        }

        // GET: Clases/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clase clase = db.Clases.Find(id);
            if (clase == null)
            {
                return HttpNotFound();
            }
            return View(clase);
        }

        // GET: Clases/Create
        public ActionResult Create()
        {
            var model = new Clase();
            model.fch_Examen = DateTime.Now;



            ViewBag.AlumnoId = new SelectList(CombosHelper.GetAlumnos(), "AlumnoId", "Nombre");
            ViewBag.MateriaId = new SelectList(CombosHelper.GetMaterias(), "MateriaId", "Nombre");
            ViewBag.ProfesorId = new SelectList(CombosHelper.GetProfesors(), "ProfesorId", "Nombre");
            return View(model);
        }

        // POST: Clases/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClaseId,AlumnoId,ProfesorId,MateriaId,fch_Examen,Promedio")] Clase clase)
        {
            if (ModelState.IsValid)
            {
                db.Clases.Add(clase);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AlumnoId = new SelectList(CombosHelper.GetAlumnos(), "AlumnoId", "Nombre", clase.AlumnoId);
            ViewBag.MateriaId = new SelectList(CombosHelper.GetMaterias(), "MateriaId", "Nombre", clase.MateriaId);
            ViewBag.ProfesorId = new SelectList(CombosHelper.GetProfesors(), "ProfesorId", "Nombre", clase.ProfesorId);
            return View(clase);
        }

        // GET: Clases/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var clase = new Clase();
            clase = db.Clases.Find(id);
            clase.fch_Examen = DateTime.Now;

            if (clase == null)
            {
                return HttpNotFound();
            }
            ViewBag.AlumnoId = new SelectList(CombosHelper.GetAlumnos(), "AlumnoId", "Nombre", clase.AlumnoId);
            ViewBag.MateriaId = new SelectList(CombosHelper.GetMaterias(), "MateriaId", "Nombre", clase.MateriaId);
            ViewBag.ProfesorId = new SelectList(CombosHelper.GetProfesors(), "ProfesorId", "Nombre", clase.ProfesorId);
            return View(clase);
        }

        // POST: Clases/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ClaseId,AlumnoId,ProfesorId,MateriaId,fch_Examen,Promedio")] Clase clase)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clase).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AlumnoId = new SelectList(CombosHelper.GetAlumnos(), "AlumnoId", "Nombre", clase.AlumnoId);
            ViewBag.MateriaId = new SelectList(CombosHelper.GetMaterias(), "MateriaId", "Nombre", clase.MateriaId);
            ViewBag.ProfesorId = new SelectList(CombosHelper.GetProfesors(), "ProfesorId", "Nombre", clase.ProfesorId);
            return View(clase);
        }

        // GET: Clases/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clase clase = db.Clases.Find(id);
            if (clase == null)
            {
                return HttpNotFound();
            }
            return View(clase);
        }

        // POST: Clases/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Clase clase = db.Clases.Find(id);
            db.Clases.Remove(clase);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
