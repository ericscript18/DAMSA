﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DAMSA.Startup))]
namespace DAMSA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
