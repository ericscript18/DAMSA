﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DAMSA.Models
{
    public class DAMSAContext : DbContext
    {
        public DAMSAContext() : base("DefaultConnection")
        {

        }

        public System.Data.Entity.DbSet<DAMSA.Models.Alumno> Alumnoes { get; set; }

        public System.Data.Entity.DbSet<DAMSA.Models.Profesor> Profesors { get; set; }

        public System.Data.Entity.DbSet<DAMSA.Models.Materia> Materias { get; set; }

        public System.Data.Entity.DbSet<DAMSA.Models.Clase> Clases { get; set; }
    }
}