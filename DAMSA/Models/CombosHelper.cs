﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAMSA.Models
{
    public class CombosHelper : IDisposable
    {
        private static DAMSAContext db = new DAMSAContext();

        public static List<Alumno> GetAlumnos()
        {
            var alumnos = db.Alumnoes.ToList();
            alumnos.Add(new Alumno
            {
                AlumnoId = 0,
                Nombre = "[Seleccione un Alumno...]"
            });
            return alumnos.OrderBy(a => a.Nombre).ToList();
        }

        public static List<Profesor> GetProfesors()
        {
            var profesores = db.Profesors.ToList();
            profesores.Add(new Profesor
            {
                ProfesorId = 0,
                Nombre = "[Seleccione un Profesor...]"
            });
            
            return profesores.OrderBy(p => p.Nombre).ToList();
        }

        public static List<Materia> GetMaterias()
        {
            var materias = db.Materias.ToList();
            materias.Add(new Materia
            {
                MateriaId = 0,
                Nombre = "[Seleccione una Materia...]"
            });
            return materias.OrderBy(m => m.Nombre).ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}