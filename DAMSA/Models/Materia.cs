﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DAMSA.Models
{
    public class Materia
    {
        [Key]
        public int MateriaId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [MaxLength(50, ErrorMessage = "El campo {0} debe tener maximo {1} caracteres.")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0}")]
        public int ProfesorId { get; set; }


        public byte Activo { get; set; }

        [Display(Name = "Fecha Alta")]
        [Required(ErrorMessage = "Debe ingresar una {0}.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime fch_Alta { get; set; }

        [Display(Name = "Fecha Modificacion")]
        [Required(ErrorMessage = "Debe ingresar una {0}.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime fch_Mod { get; set; }


        public virtual ICollection<Clase> Clases { get; set; }
    }
}