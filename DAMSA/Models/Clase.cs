﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DAMSA.Models
{
    public class Clase
    {
        [Key]
        public int ClaseId { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0}")]
        public int AlumnoId { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0}")]
        public int ProfesorId { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0}")]
        public int MateriaId { get; set; }

        [Display(Name = "Fecha Examen")]
        [Required(ErrorMessage = "Debe ingresar una {0}.")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime fch_Examen { get; set; }

        [Display(Name = "Promedio")]
        [Required(ErrorMessage = "Debe ingresar {0},")]
        [Range(5.0, 10.0, ErrorMessage = "El promedio deber ser entre {1} y {2}")]
        public double Promedio { get; set; }

        public virtual Alumno Alumno { get; set; }
        public virtual Profesor Profesor { get; set; }
        public virtual Materia Materia { get; set; }
    }
}